package com.demo;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Slf4j
@Path("/userinfo")
public class UserInfoResource {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserInfo hello(UserInfo userInfo) {
        log.info("Received : " + userInfo);
        return userInfo;
    }
}